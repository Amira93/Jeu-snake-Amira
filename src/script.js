import { Apple } from "./apple";
import { snake } from "./snake";
let jouer = document.querySelector(".jouer");
    jouer.style.display = 'block';


/**
 * @param {string} canvasWidth
 * @param {string} canvasHeight
 * *@param {string} score
 */

let canvasWidth = 800;
let canvasHeight = 500;
let delai = 90;
let ctx;
let snakee; //serpent
let applee // pomme
let score;

(function init() {
    let canvas = document.querySelector('#canvas')
    canvas.width = canvasWidth;
    canvas.height = canvasHeight;
    //cree le contexte //serpent
    ctx = canvas.getContext('2d');
    snakee = new snake([[6, 4], [5, 4], [4, 4], [3, 4], [2, 4]], "right");//crée le serpent et sa direction a l'initialisation
    applee = new Apple([10, 10]);
    score = 0;
    refreshCanvas();

})()

function refreshCanvas()  //refrichir le canvas
{

    snakee.advance()
    if (snakee.checkCollision()) {
        ctx.clearRect(0, 0, canvasWidth, canvasHeight)
         gameOver();

    }
    else {
        if (snakee.isEatingApple(applee))    //si le serpent mange la pomme
        {
            score++;
            snakee.eatApple = true;
            applee.setNewPosition();        // changer la position de la pomme
        }
        ctx.clearRect(0, 0, canvasWidth, canvasHeight)
        drawScore();
        snakee.draw();
        applee.draw();

        setTimeout(refreshCanvas, delai) //éxecute la fonction chaque fois que le temps donné "delai" est passé
    }

}
function gameOver() {
    ctx.font = "bold 70px sans-serif";
    ctx.fillStyle = "red";
    ctx.textAlign = "center";
    ctx.strokeStyle = "white";
    ctx.lineWidth = 5;
    let centreX = canvasWidth / 2;
    let centreY = canvasHeight / 2;
    ctx.strokeText("Game Over", centreX, centreY - 40);
    ctx.fillText("Game Over", centreX, centreY - 40);


    ctx.lineWidth = 4;
    ctx.font = "bold 30px sans-serif";
    ctx.fillStyle = "#33cc33";
    ctx.strokeText("Appuyer sur la touche Espace pour rejouer", centreX, centreY + 40);
    ctx.fillText("Appuyer sur la touche Espace pour rejouer", centreX, centreY + 40);
}
function drawScore() {

    ctx.font = "bold 80px sans-serif";
    // ctx.fillStyle = "rgb(65, 50, 50)";
    ctx.strokeStyle = " black;";
    ctx.lineWidth = 5;
    ctx.textAlign = "left";
    ctx.strokeText(score.toString(), 380, 480);

}

function start() {

    if (jouer.style.display === 'block')
    {
        jouer.style.display = 'none';
        canvas.style.display = 'block';
    }
}
jouer.addEventListener("click",()=> {
    start()
});

function restart() {
    snakee = new snake([[6, 4], [5, 4], [4, 4], [3, 4], [2, 4]], "right");
    applee = new Apple([10, 10]);
    score = 0;
    clearTimeout();
    refreshCanvas();
}

document.onkeydown = function handleKeyDown(e) {
    var key = e.keyCode;  //touche appuyée
    var newDirection;
    switch (key) {
        case 37:
            newDirection = "left";
            break;
        case 38:
            newDirection = "up";
            break;
        case 39:
            newDirection = "right";
            break;
        case 40:
            newDirection = "down";
            break;
        case 32:
            restart();
            return;
            case 13:
            start();
            return;
        default:
            return;

    }
    snakee.setDirection(newDirection);
}






