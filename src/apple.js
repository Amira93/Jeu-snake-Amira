export class Apple{

  /**
   * @param {number} blocksize
   */
    blockSize = 20;
    ctx = canvas.getContext('2d');


    /**
     * 
     * @param {position[]} position 
     */
    constructor (position){
    this.position = position;
    
    }

    draw() {
        this.ctx.fillStyle = "red";
        this.ctx.beginPath();
        let radius = this.blockSize / 2;  //taille de la pomme 
        let x = this.position[0] * this.blockSize + radius; //atteindre le point central du block pour tracer le cercle
        let y = this.position[1] * this.blockSize + radius;
        this.ctx.arc(x, y, radius, 0, Math.PI * 2, true);
        this.ctx.fill();  //pour remplir le cercle
       };
    
    setNewPosition() {
        let newX = Math.round(Math.random() * 39);  //position aléatoire 
        let newY = Math.round(Math.random() * 24);
        this.position = [newX, newY];
    };

}