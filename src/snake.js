export class snake   
{
    /**
     * @param {number} blocksize
     * @param {number} widthInBlocks 
     * @param {number} heightInBlocks  
     */


      canvasWidth = 800;
      canvasHeight = 500;
      blockSize = 20; //taille des blocks
      widthInBlocks = this.canvasWidth/this.blockSize;
      heightInBlocks = this.canvasHeight/this.blockSize;
      ctx  = canvas.getContext('2d');
     
      /**
       * 
       * @param {body[]} body 
       * @param {string} direction 
       */
    constructor(body, direction) {
        this.body = body;
        this.direction = direction;
        this.aetApple = false;
        
    }
    
    
    draw() {
         
         this.ctx.fillStyle = '#33cc33';
         for ( let i = 0; i < this.body.length; i++) //passer sur chacun des membres du body du serpent
        {
            this.drawBlock( this.ctx, this.body[i]);
        }
         
    }


    // methode advance pour faire avancer le serpent  
    advance() {
        let nextPosition = this.body[0].slice();  //crée un vouvel élement à partir de format copié
        switch (this.direction) {
            case "left":
                nextPosition[0] -= 1;
                break;
            case "right":
                nextPosition[0] += 1;
                break;
            case "down":
                nextPosition[1] += 1;
                break;
            case "up":
                nextPosition[1] -= 1;
                break;
            default:

        }
        this.body.unshift(nextPosition);   //rajouter un élement a la premiére position du array

        if (!this.eatApple)
            this.body.pop(); //enléve le dernier élement du array
        else
            this.eatApple = false;
    };

     setDirection(newDirection) {
        let allowdDirection;      //direction permise
         switch(this.direction)
        {
        case "left":
        case "right":
        allowdDirection = ["up", "down"];
        break;
        case "down":
        case "up":
        allowdDirection = ["left", "right"];
        break;
            default:
        }
        if(allowdDirection.indexOf(newDirection) > -1)
            {
                this.direction = newDirection;
            }
         
    };

    checkCollision() {
         let wallCollision = false;
         let snakeCollision = false;
         let head = this.body[0];
         let rest = this.body.slice(1);
         let snakeX = head[0];
         let snakeY = head[1];
         let minX = 0;
         let minY = 0;
         let maxX = this.widthInBlocks - 1;
         let maxY = this.heightInBlocks - 1;
         let isNoteHorizontal = snakeX < minX || snakeX > maxX ;
         let isNoteVertical = snakeY < minY || snakeY > maxY ;
        if (isNoteHorizontal || isNoteVertical)
            {
                wallCollision = true;
            }
        for( let i = 0; i < rest.length; i++)
            {
                if (snakeX === rest[i][0] && snakeY === rest[i][1])
                    {
                        snakeCollision = true;
                    }
            }
              return wallCollision || snakeCollision;
        
    };
    isEatingApple(appleToEat) {
         let head = this.body[0];
        if (head[0] === appleToEat.position[0] && head[1] === appleToEat.position[1])
            return true;
        else
            false;
    }

    drawBlock(ctx, position) {
        let x = position[0] * this.blockSize;
        let y = position[1] * this.blockSize;
         this.ctx.fillRect(x, y, this.blockSize, this.blockSize);// remplir un block à la taille de blockSize
        
    }
}