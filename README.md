## Presentation de projet jeu
Projet ‘’Jeu de serpent’’
Date : 02 juin 2021
Réalisateur : Amira Habi

### Description :

Ce projet entre dans le cadre de la formation ‘Développeur Web’ de l’école Simplon.co Lyon. Le concept de cette formation est de suivre des cours théoriques et des ateliers pratiques puis de les valider par des projets personnels.
Ce projet personnel a été réalisé en HTML avec la balise "canvas", un peu de CSS et avec du JavaScript.
Le snake (ou jeu du serpent) est un jeu vidéo dans lequel le joueur dirige une ligne qui grandit et constitue ainsi elle-même un obstacle.

Les règles sont simples : vous dirigez un serpent avec les flèches de votre clavier qui va grossir à chaque fois que vous lui ferez manger une pomme . Le but est de manger le plus de pommes rouge possible sans que le serpent se retrouve bloqué par sa propre queue ou sur le mur.

## la maquette 
![wireframe jeu](image/maquette3.png)
![wireframe jeu](image/maquette1.png)
![wireframe jeu](image/maquette2.png)

## le lien de gitlab:
https://amira93.gitlab.io/Jeu-snake-Amira
